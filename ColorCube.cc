#include "ColorCube.h"
#include <glut.h>

#define WINDOW_WIDTH	650
#define WINDOW_HEIGHT	650

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( WINDOW_WIDTH, WINDOW_HEIGHT );
	
	glutCreateWindow( "Color Cube " );
	//glutFullScreen( );
	//glutInitDisplayString( "Color Cube Slider" ); 
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );
	
	glEnable( GL_BLEND );
	glDisable( GL_CULL_FACE );
	//glEnable( GL_LINE_STIPPLE );

	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glShadeModel( GL_SMOOTH );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

	glutMainLoop( );

	return 0;
}

void render( )
{
	glLoadIdentity( );
	

	glTranslatef( 0.25f, -0.5f, -0.0f );
	glRotatef( -45.0f, 0.0f, 1.0f, 0.0f );
	glRotatef( 45.0f, 0.0f, 0.0f, 1.0f );
	//glScalef( 0.5f, 0.5f, 0.5f );

	glPointSize( 3.0f );

	
	glColor3f( 1.0f, 0.0f, 0.0f );
	glBegin( GL_QUADS );
		glColor3f( 0.0f, 0.0f, 0.0f ); glVertex3f( 0.0f, 0.0f, 0.0f ); 
		glColor3f( 1.0f, 0.0f, 0.0f ); glVertex3f( 1.0f, 0.0f, 0.0f ); 
		glColor3f( 1.0f, 1.0f, 0.0f ); glVertex3f( 1.0f, 1.0f, 0.0f ); 
		glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 0.0f ); 

		glColor3f( 0.0f, 0.0f, 1.0f ); glVertex3f( 0.0f, 0.0f, 1.0f ); 
		glColor3f( 0.0f, 0.0f, 0.0f ); glVertex3f( 0.0f, 0.0f, 0.0f ); 
		glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 0.0f ); 
		glColor3f( 0.0f, 1.0f, 1.0f ); glVertex3f( 0.0f, 1.0f, 1.0f ); 

		glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 0.0f );
		glColor3f( 1.0f, 1.0f, 0.0f ); glVertex3f( 1.0f, 1.0f, 0.0f );
		glColor3f( 1.0f, 1.0f, 1.0f ); glVertex3f( 1.0f, 1.0f, 1.0f );
		glColor3f( 0.0f, 1.0f, 1.0f ); glVertex3f( 0.0f, 1.0f, 1.0f );
	glEnd( );

	glEnable( GL_LINE_STIPPLE );
	glPushAttrib( GL_COLOR_BUFFER_BIT );
	glColor3f( 0, 0, 0 );
	glLineStipple( 1, 0x0101 );
	glBegin( GL_LINES );
		glVertex3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 1.0f, 1.0f, 0.0f );
		glVertex3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, 0.0f, 0.0f );
		glVertex3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 1.0f );
	glEnd( );
	glPopAttrib( );
	glDisable( GL_LINE_STIPPLE );

	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("Red <1,0,0>"), 550, 380 );
	writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("Green <0,1,0>"), 250, 390 );
	writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("Blue <0,0,1>"), 150, 150 );
	writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("Black <0,0,0>"), 400, 150 );
	writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("White <1,1,1>"), 130, 630 );
	//writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("red <1,0,0>"), 453, 345 );
	//writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("red <1,0,0>"), 453, 345 );
	//writeStrokeText( GLUT_STROKE_MONO_ROMAN, std::string("red <1,0,0>"), 453, 345 );

	writeBitmapText( GLUT_BITMAP_HELVETICA_18, std::string("Color Cube"), 20, 35 );
	writeBitmapText( GLUT_BITMAP_HELVETICA_10, std::string("Press the 'Q' key to exit."), 20, 20 );
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	if( height <= 0 ) height = 1;
	double aspect = height / width;
	glViewport( 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	//glOrtho( -1, 1, -1, 1, -1, 1 );
	//glFrustum( -1.0, 1.0, -1.0, 1.0, -5.0, 5.0 );
	glMatrixMode( GL_MODELVIEW );

}

void keyboard_keypress( unsigned char key, int x, int y )
{
	int a = 0;
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			exit( 0 );
		default:
			break;
	}

}

void writeBitmapText( void *font, std::string &text, int x, int y )
{
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	glDisable( GL_DEPTH_TEST );
	glDisable( GL_TEXTURE_2D );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, width, 0, height, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glRasterPos2i( x, y );

			for( int i = 0; i < text.size( ); i++ )
				glutBitmapCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	glEnable( GL_DEPTH_TEST );

}

void writeStrokeText( void *font, std::string &text, int x, int y )
{
	//int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	//int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	glDisable( GL_DEPTH_TEST );
	glDisable( GL_TEXTURE_2D );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glTranslatef( x, y, 0 );
			glScalef( 0.08f, 0.08f, 0.08f );

			for( int i = 0; i < text.size( ); i++ )
				glutStrokeCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	glEnable( GL_DEPTH_TEST );

}



void idle( )
{
	glutPostRedisplay( );
}