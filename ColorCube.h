#include <string>

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void idle( );
void writeBitmapText( void *font, std::string &text, int x, int y );
void writeStrokeText( void *font, std::string &text, int x, int y );